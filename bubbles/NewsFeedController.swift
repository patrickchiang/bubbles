import UIKit

class NewsFeedController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    struct NewsFeedItem {
        var transID: String
        var transUser: String
        var transAvatar: UIImage
        var transText: String
    }
    
    let feed: [NewsFeedItem] = [
        NewsFeedItem(transID: "11", transUser: "otherguy123", transAvatar: UIImage(named: "user_male3-32.png"), transText: "You sold 13 units of sugar to otherguy123 for $100"),
        NewsFeedItem(transID: "12", transUser: "otherguy123", transAvatar: UIImage(named: "user_male3-32.png"), transText: "You sold 18 units of sugar to otherguy123 for $130"),
        NewsFeedItem(transID: "13", transUser: "otherguy123", transAvatar: UIImage(named: "user_male3-32.png"), transText: "You bought 10 units of corn from otherguy123 for $40")
    ]
    let news: [NewsFeedItem] = []
    
    @IBOutlet var navTitle: UINavigationItem
    @IBOutlet var table: UITableView
    
    override func viewDidLoad() {
        navTitle.title = "News Feed"
    }
    
    func numberOfSectionsInTableView(tableView: UITableView!) -> Int {
        return 2
    }
        
    func tableView(tableView: UITableView!, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return feed.count
        } else {
            return news.count
        }
    }
    
    func tableView(tableView: UITableView!, titleForHeaderInSection section: Int) -> String! {
        if section == 0 {
            return "Your Feed"
        } else {
            return "Global Feed"
        }
    }
    
    
    func tableView(tableView: UITableView!, cellForRowAtIndexPath indexPath: NSIndexPath!) -> UITableViewCell! {
        var cell: NewsFeedTableCell = tableView.dequeueReusableCellWithIdentifier("rows", forIndexPath: indexPath) as NewsFeedTableCell
        
        var feedArray: [NewsFeedItem]
        
        if indexPath.section == 0 {
            feedArray = feed
        } else {
            feedArray = news
        }
        
        cell.rowName = feedArray[indexPath.row].transText
        cell.transactionID = feedArray[indexPath.row].transID
        cell.avatarImage = feedArray[indexPath.row].transAvatar
                
        // Change selected bg color
        var selectedView: UIView = UIView()
        selectedView.backgroundColor = UIColor.darkGrayColor()
        cell.selectedBackgroundView = selectedView
        
        return cell
    }
    
    func tableView(tableView: UITableView!, didSelectRowAtIndexPath indexPath: NSIndexPath!) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        if segue!.identifier == "showTransaction" {
            var newVC = segue!.destinationViewController as TransactionController;
            var senderCell = sender as NewsFeedTableCell
            newVC.titleText = senderCell.rowName
            newVC.titleID = senderCell.transactionID
        }
    }
}