import UIKit

class TransactionController: UIViewController {
    @IBOutlet var titleLabel: UILabel
    @IBOutlet var navTitle: UINavigationItem

    var titleText: String = ""
    var titleID: String = ""
    
    override func viewDidLoad() {
        titleLabel.text = titleText
        navTitle.title = "Transaction " + titleID
    }
}