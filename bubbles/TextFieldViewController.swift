import UIKit

class TextFieldViewController: UIViewController, UITextFieldDelegate {
        
    func textFieldShouldReturn(textField: UITextField!) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
