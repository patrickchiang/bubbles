import UIKit

class NewsFeedTableCell: UITableViewCell {
    @IBOutlet var rowLabel: UILabel
    @IBOutlet var rowAvatar: UIImageView
    
    var rowName: String {
        get {
            return rowLabel.text
        }
        set (newLabel) {
            rowLabel.text = newLabel
            rowLabel.sizeToFit()
        }
    }
    
    var transactionID: String = ""
    
    var avatarImage: UIImage {
        get{
            return rowAvatar.image
        }
        set (newImage) {
            rowAvatar.image = newImage
        }
    }
}