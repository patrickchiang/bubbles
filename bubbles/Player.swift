import Foundation

class Player: NSObject {
    var username: String
    var shares: Dictionary<Resource, Int> = [:]
    
    init(username: String) {
        self.username = username
    }
}
