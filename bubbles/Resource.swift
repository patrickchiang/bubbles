import UIKit

class Resource: NSObject {
    var displayName: String
    var iconImage: UIImage
    
    init(displayName: String, imageName: String) {
        self.displayName = displayName
        iconImage = UIImage(named: imageName)
    }
    
}
