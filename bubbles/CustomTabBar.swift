import UIKit

class CustomTabBar: UITabBarController {
    override func viewDidLoad() {
        self.tabBar.barStyle = UIBarStyle.Black
        self.tabBar.tintColor = UIColor.whiteColor()
    }
}